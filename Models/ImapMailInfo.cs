// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using MailKit;

namespace ImapSync.Models;

public partial class ImapMailInfo : ObservableObject, INotifyPropertyChanged
{
    public const int MarginLeft = -16;

    [ObservableProperty]
    private bool _isLoaded = false;

    [ObservableProperty]
    private string _name = String.Empty;

    [ObservableProperty]
    private string _status = "Actualizando datos...";

    [ObservableProperty]
    private ulong _size = 0;

    [ObservableProperty]
    private int _count = 0;

    private bool _isProccesed = false;

    private int _level = 1;

    private IMailFolder _folder;

    public List<ImapMailInfo> Children { get; set; } = new();

    public Thickness NegativeIndent { get; set; }

    public int Level
    {
        get => _level;
        set
        {
            _level = value;
            NegativeIndent = new Thickness(MarginLeft * (_level - 0.2), 0, 0, 0);
        }
    }

    public IMailFolder Folder
    {
        get => _folder;
        set
        {
            if (value != null)
            {
                _folder = value;
                Name = _folder.Name;
                Count = _folder.Count;

                if (_folder.Size != null)
                {
                    Size = (ulong) _folder.Size;
                }
            }
        }
    }

    partial void OnIsLoadedChanged(bool value)
    {
        if (Children.Count > 0)
        {
            if (!_isProccesed && IsLoaded)
            {
                ProccesedSizeAndCount();
            }
        }
    }

    private void ProccesedSizeAndCount()
    {
        if (Children.Count == 0)
        {
            Debug.WriteLine("Error to procced size and count for folders:", Folder?.FullName);

            return;
        }

        if (!_isProccesed)
        {
            Size = 0;
            Count = 0;

            foreach (ImapMailInfo folder in Children)
            {
                if (folder.Folder != null)
                {
                    Size += folder.Size;
                }

                Count += folder.Count;
            }

            _isProccesed = true;
        }
    }
}
