// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

namespace ImapSync.Models;

public class AppConfig
{
    public string ConfigurationsFolder { get; set; }

    public string AppPropertiesFileName { get; set; }
}
