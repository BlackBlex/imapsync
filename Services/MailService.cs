// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using ImapSync.Helpers.Extensions;
using ImapSync.Models;
using MailKit;
using Newtonsoft.Json;

namespace ImapSync.Services;

public class MailService : IDisposable
{
    private readonly BackgroundWorker _worker = new();
    private readonly Dictionary<string, List<IMailFolder>> _mailsDictionary = new();
    private readonly ImapConnectionService _imapConnectionService;

    private List<MailInfo> _mails;

    public MailService(ImapConnectionService imapConnectionService)
    {
        _imapConnectionService = imapConnectionService;
    }

    public void FetchMails()
    {
        if (_mails == null || _mails?.Count == 0)
        {
            List<string> files = GetConfigFiles();
            _mails = new List<MailInfo>();

            foreach (string file in files)
            {
                string json = File.ReadAllText(file);

                if (!String.IsNullOrWhiteSpace(json))
                {
                    MailInfo? info = JsonConvert.DeserializeObject<MailInfo>(json);
                    if (info != null)
                    {
                        info.ImapInfo.Name = $"{info.User}@{info.Host}";
                        _mails.Add(info);
                    }
                }
            }
        }
    }

    public List<MailInfo> GetMails() => _mails;

    public void BuildConnections()
    {
        if (!_worker.IsBusy)
        {
            _worker.WorkerReportsProgress = true;
            _worker.DoWork += MakeConnectionBackground;

            _worker.RunWorkerAsync();
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            _worker.Dispose();
        }
    }

    private static List<string> GetConfigFiles() => Directory.GetFiles("./Config").ToList();

    private void MakeConnectionBackground(object? sender, DoWorkEventArgs eventArgs)
    {
        IMailFolder? folder;
        int total = _mails.Count;
        int current = 0;
        var watch = Stopwatch.StartNew();

        foreach (MailInfo mail in _mails)
        {
            string username = $"{mail.User}@{mail.Host}";
            current++;

            (ImapConnection connection, TimeSpan time, string? error) = _imapConnectionService.Build(mail.Host, mail.Port);
            _worker.ReportProgress(current * 100 / total, $"Actualizando datos de: {mail.User}@{mail.Host}");

            if (!String.IsNullOrEmpty(error))
            {
                mail.ImapInfo.Status = error;
                continue;
            }

            if (!_mailsDictionary.ContainsKey(mail.User))
            {
                _mailsDictionary.Add(mail.User, new List<IMailFolder>());
            }

            (bool isSucceed, time, error) = connection.LoginTo(username, mail.Passwd);
            if (!isSucceed)
            {
                mail.ImapInfo.Status = error;
                continue;
            }

            mail.ImapConnection = connection;

            (isSucceed, time, error) = connection.ListFolders();
            if (!isSucceed)
            {
                mail.ImapInfo.Status = error;
                continue;
            }

            foreach (string folderStr in connection.GetFolders())
            {
                folder = connection.GetMailFolder(folderStr).mailbox;

                if (folder == null)
                {
                    Debug.WriteLine("Folder null: " + folderStr);
                    continue;
                }

                _mailsDictionary[mail.User].Add(folder);
            }

            ProccedMailInfo(mail);
        }

        watch.Stop();

        eventArgs.Result = "Duración: " + watch.Elapsed + ".";
    }

    private void ProccedMailInfo(MailInfo mail)
    {
        if (_mailsDictionary.TryGetValue(mail.User, out List<IMailFolder>? folders))
        {
            if (folders == null)
            {
                mail.ImapInfo.Status = "Error to get folders";
                return;
            }

            foreach (IMailFolder folder in folders)
            {
                folder.Status(StatusItems.Size | StatusItems.Count);
                if (folder.Size != null)
                {
                    if (folder.ParentFolder != null)
                    {
                        ImapMailInfo? folderToSearch = mail.ImapInfo.Children.SearchChild(folder.FullName);
                        if (folderToSearch != default)
                        {
                            folderToSearch.Children.Add(new ImapMailInfo() { Folder = folder, Level = folderToSearch.Level + 1 });
                            continue;
                        }
                    }

                    mail.ImapInfo.Children.Add(new ImapMailInfo() { Folder = folder, Level = mail.ImapInfo.Level + 1 });
                }
            }

            mail.ImapInfo.IsLoaded = true;
        }
        else
        {
            mail.ImapInfo.Status = "Error to access folders";
        }
    }
}
