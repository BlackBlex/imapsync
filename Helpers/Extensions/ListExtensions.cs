// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using ImapSync.Models;

namespace ImapSync.Helpers.Extensions;
internal static class ListExtensions
{
    public static ImapMailInfo? SearchChild(this List<ImapMailInfo> list, string folderName)
    {
        if (list == null)
        {
            return default;
        }

        return Search(list, folderName);

        ImapMailInfo? Search(List<ImapMailInfo> list, string folderName)
        {
            if (list.Count == 0 || String.IsNullOrEmpty(folderName))
            {
                return default;
            }

            string parentFolderName = folderName[..folderName.IndexOf('.')];

            ImapMailInfo? folderToSearch = list.FirstOrDefault(info => info.Name == parentFolderName);

            if (folderToSearch != default)
            {
                folderName = folderName.Replace($"{parentFolderName}.", String.Empty);
                if (folderName.IndexOf(".") != -1)
                {
                    folderToSearch = Search(folderToSearch.Children, folderName);
                }
            }

            return folderToSearch;
        }
    }
}
