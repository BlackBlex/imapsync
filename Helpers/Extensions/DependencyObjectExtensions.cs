// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace ImapSync.Helpers.Extensions;
internal static class DependencyObjectExtensions
{
    public static T? VisualUpwardSearch<T>(this DependencyObject source)
        where T : DependencyObject
    {
        DependencyObject returnVal = source;

        while (returnVal is not null and not T)
        {
            DependencyObject? tempReturnVal = null;
            if (returnVal is Visual or Visual3D)
            {
                tempReturnVal = VisualTreeHelper.GetParent(returnVal);
            }

            returnVal = tempReturnVal ?? LogicalTreeHelper.GetParent(returnVal);
        }

        return returnVal as T;
    }
}
