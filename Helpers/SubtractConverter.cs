// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Windows.Data;
using System.Windows.Markup;

namespace ImapSync.Helpers;

[ValueConversion(typeof(double), typeof(double))]
[MarkupExtensionReturnType(typeof(SubtractConverter))]
public class SubtractConverter : MarkupExtension, IValueConverter
{
    public double Value { get; set; }

    public object Convert(object baseValue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        double val = System.Convert.ToDouble(baseValue);

        // Change here if you want other operations
        return val - Value;
    }

    public object? ConvertBack(object baseValue, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        return null;
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
        return this;
    }
}
